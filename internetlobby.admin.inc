<?php

function internetlobby_admin($form, &$form_state) {
  $form['internetLobby_AdminEmail'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrator e-mail:'),
    '#default_value' => variable_get('internetLobby_AdminEmail'),
    '#description' => t('All the feedback will be sent on this e-mail.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

function internetlobby_admin_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['internetLobby_AdminEmail'])) {
    form_set_error('email', t('Please enter a valid e-mail!'));
  }
}